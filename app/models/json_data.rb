# frozen_string_literal: true

require 'json'
# Just in case to run from console
# This class handles data from JSON file, and has methods to search thought data
class JsonData
  @@data = JSON.parse(File.read('data.json'))
  class << self
    def find(term)
      # initial array with results
      result = []
      # decide what to do with query term
      case term
      when /".+"/i
        # exact search
        result << exact_match_search(term)
      when /--.+/i
        # negative search
        result << negative_search(term)
      else
        # full_search
        # start full words search
        result << full_search(term)
        # add results of reversed words search
        result << reversed_word_search(term)
        # add results for matching in different fields
        result << different_field_search(term)
      end
      # lets return one dimensional array, where no duplicated items
      result.flatten.uniq
    end

    def full_search(term)
      @@data.select do |item|
        item['Name'] =~ Regexp.new(Regexp.escape(term), true) ||
          item['Type'] =~ Regexp.new(Regexp.escape(term), true) ||
          item['Designed by'] =~ Regexp.new(Regexp.escape(term), true)
      end
    end

    def reversed_word_search(term)
      full_search(term.split(' ').reverse.join(' '))
    end

    def exact_match_search(term)
      # extract exact term
      # exact_term = term[/"([^"]*)"/, 1] (moved to separate method during refactoring)
      # extract rest of term and remove spaces from beggining and end of string
      # rest_term = term.gsub(/"([^"]*)"/, '').strip (moved to separate method during refactoring)
      # lets see what we got with rest of term
      # full_result = full_search(rest_of_term(term))
      # and what we got with exact term
      # exact_result = full_search(extract_term(term))
      # in intersection we will have results which has exact term and rest of term
      # full_result & exact_result
      # lets make it in one line =)
      full_search(rest_of_exact_term(term)) & full_search(extract_exact_term(term))
    end

    def different_field_search(term)
      # make empty array with temporary results
      result = []
      # long form
      # term.split.each do |s|
      #  result << full_search(s)
      # end
      # short one
      term.split.each { |t| result << full_search(t) }
      # make intersection of results
      result.inject(:&)
    end

    def negative_search(term)
      # temp resulting array
      result = []
      # search to all items which meet search criteria
      result << full_search(rest_of_term_after_excluding(term))
      # search for all items which should be excluded
      extract_excluding_term(term).split.each { |t| result << full_search(t) }
      # return items after exclusion
      result.inject(:-)
    end

    private

    def extract_exact_term(term)
      term[/"([^"]*)"/, 1]
    end

    def rest_of_exact_term(term)
      term.gsub(/"([^"]*)"/, '').strip
    end

    def extract_excluding_term(term)
      term[/--(.*)/, 1]
    end

    def rest_of_term_after_excluding(term)
      term.gsub(/--(.*)/, '').strip
    end
  end
end
