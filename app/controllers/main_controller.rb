class MainController < ApplicationController
  def index
  end

  def search
    @results = JsonData.find(params[:term])
    @query = params[:term]
    respond_to do |format|
      #format.html { redirect_to root_url }
      format.js #default behaviour is to run app/views/notes/create.js.erb file
    end

  end
end
