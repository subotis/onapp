# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.5.1

* Rails version - 5.2.1

* To Run - cd to folder and run bundle install, then go to localhost:3000


**Wanna run it thought console? - No problems:**

* cd into app dir
* type 'irb'
* type require_relative 'app/models/json_data.rb'
* use it JsonData.find('your query string')
